import { FC } from 'react'
import { Card, Col, Row, Space } from 'antd'
import View from '@/components/View'
import ProgressBar from '@/components/ProgressBar'

const ProgressPage: FC = () => {
  const count1 = 26.5
  const count2 = 33
  const count3 = 60
  const count4 = 100

  return (
    <View className="app-page-progress">
      <Card type="inner" title="进度条">
        <Row gutter={[24, 24]}>
          <Col span="12">
            <Space className="flex" direction="vertical" size={20}>
              <div className="progress">
                <div className="flex">
                  <span className="mr-5 font-bold">当前完成量</span>
                  <ProgressBar label={`${count1} %`} title="当前完成量" value={count1} />
                </div>
              </div>
              <div className="progress">
                <div className="flex">
                  <span className="mr-5 font-bold">已经完成量</span>
                  <ProgressBar classes="success" label={`${count2} %`} title="已经完成量" value={count2} />
                </div>
              </div>
              <div className="progress">
                <div className="flex">
                  <span className="mr-5 font-bold">剩余完成量</span>
                  <ProgressBar classes="warning" label={`${count3} %`} title="剩余完成量" value={count3} />
                </div>
              </div>
              <div className="progress">
                <div className="flex">
                  <span className="mr-5 font-bold">超额完成量</span>
                  <ProgressBar classes="danger" label={`${count4} %`} title="超额完成量" value={count4} />
                </div>
              </div>
            </Space>
          </Col>
        </Row>
      </Card>
    </View>
  )
}

export default ProgressPage

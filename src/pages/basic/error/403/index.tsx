import { FC } from 'react'
import { Button, Card, Result, notification } from 'antd'
import View from '@/components/View'

const Page403: FC = () => {
  const handleClick = () => {
    notification.success({
      message: '您点击了上一页',
    })
  }
  return (
    <View>
      <Card type="inner" className="app-page-404" title="403">
        <Result
          status="403"
          title="403"
          subTitle="Sorry, you are not authorized to access this page."
          extra={<Button type="primary" onClick={handleClick}>返回上一页</Button>}
        />
      </Card>
    </View>
  )
}

export default Page403

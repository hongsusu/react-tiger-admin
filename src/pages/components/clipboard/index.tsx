import { FC } from 'react'
import { Card, Col, Form, Row, Tooltip } from 'antd'
import View from '@/components/View'
import Clipboard from '@/components/Clipboard'
import './index.less'

const layout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 19 },
}
const ClipboardPage: FC = () => {
  const data = { code: Date.now().toString() }
  return (
    <View className="app-page">
      <Card type="inner" title="剪切板">
        <Row>
          <Col span={6}>
            <Form {...layout} name="basic" className="app-device-form">
              <Form.Item label="用户ID:" name="code">
                <div className="form-fields">
                  <Tooltip overlayClassName="fields-tooltip" title={data.code}>
                    <span className="fields">{data.code}</span>
                  </Tooltip>
                  <span className="fields-clip">
                    <Clipboard text={data.code} />
                  </span>
                </div>
              </Form.Item>
            </Form>
          </Col>
          <Col span={6}>
            <Form {...layout} name="basic" className="app-device-form">
              <Form.Item label="用户ID:" name="code">
                <div className="form-fields">
                  <Tooltip overlayClassName="fields-tooltip" title={data.code}>
                    <span className="fields">{data.code}</span>
                  </Tooltip>
                  <span className="fields-clip">
                    <Clipboard text={data.code} />
                  </span>
                </div>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </Card>
    </View>
  )
}

export default ClipboardPage

import { FC } from 'react'
import { Tabs, Card } from 'antd'
import View from '@/components/View'
import AntIcon from './ant-icon'
import './index.less'

const IconPage: FC = () => {
  console.log('window', window)
  return (
    <View className="app-page-icon">
      <Tabs defaultActiveKey="1" type="card">
        <Tabs.TabPane tab="AntDeisng" key="1">
          <Card>
            <AntIcon />
          </Card>
        </Tabs.TabPane>
        <Tabs.TabPane tab="CarbonDesign" key="2">
          <Card>
            <iframe src="https://carbondesignsystem.com/guidelines/icons/library/" width="100%" style={{ height: 'calc(100vh - 200px)' }} title="test" frameBorder="0" />
          </Card>
        </Tabs.TabPane>
      </Tabs>
    </View>
  )
}

export default IconPage

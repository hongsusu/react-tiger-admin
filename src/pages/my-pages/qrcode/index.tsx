import { FC } from 'react'
import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import QRCode from 'qrcode.react'
import { downLoadQRcode } from '@/utils'
import { useSetState } from 'ahooks'

const Qrcode: FC = () => {
  const currentUrl = window.location.href
  const [state, setState] = useSetState({ url: 'http://www.baidu.com', url2: 'http://www.sina.com' })
  const [form] = Form.useForm()
  const { validateFields } = form
  // const logoUrl = 'https://dss2.bdstatic.com/5bVYsj_p_tVS5dKfpU_Y_D3/res/r/image/2021-3-4/hao123%20logo.png'

  const onCreate = () => {
    validateFields().then((values: any) => {
      setState({ url: values })
    })
  }

  const onDownload = (id: string) => {
    downLoadQRcode(id)
  }

  return (
    <div className="app-page-qrcode">
      <Form
        className="app-vertical-form"
        name="basic"
        layout="vertical"
        form={form}
        preserve={false}
        requiredMark={false}
        initialValues={{
          url: 'http://www.baidu.com',
        }}
      >
        <Row gutter={[24, 24]}>
          <Col span={8}>
            <Card title="基础实例" className="text-center" bordered={false}>
              <div className="mb-5">
                <Space>
                  <Input value={currentUrl} style={{ width: '300px' }} disabled />
                  <Button type="primary" onClick={() => onDownload('js-qrcode-1')}>下载</Button>
                </Space>
              </div>
              <QRCode
                id="js-qrcode-1"
                value={currentUrl}// 生成二维码的内容
                size={200} // 二维码的大小
                fgColor="#000000"
              />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="动态生成" className="text-center" bordered={false}>
              <div className="mb-5">
                <Space>
                  <Form.Item name="url" noStyle>
                    <Input style={{ width: '300px' }} />
                  </Form.Item>
                  <Button style={{ backgroundColor: 'green', color: 'white' }} onClick={onCreate}>生成</Button>
                  <Button type="primary" onClick={() => onDownload('js-qrcode-2')}>下载</Button>
                </Space>
              </div>
              <QRCode
                id="js-qrcode-2"
                value={state.url} // 生成二维码的内容
                size={200} // 二维码的大小
                fgColor="#000000"
              />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="合成Logo" className="text-center" bordered={false}>
              <div className="mb-5">
                <Space>
                  <Button type="primary" onClick={() => onDownload('js-qrcode-3')}>下载</Button>
                </Space>
              </div>
              <QRCode
                id="js-qrcode-3"
                value={state.url2} // 生成二维码的内容
                size={200} // 二维码的大小
                fgColor="#000000"
                imageSettings={{ // 二维码中间的logo图片
                  src: '/react.svg',
                  height: 45,
                  width: 45,
                  excavate: true, // 中间图片所在的位置是否镂空
                }}
              />
            </Card>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default Qrcode

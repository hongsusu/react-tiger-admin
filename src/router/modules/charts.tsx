import { lazy } from 'react'
import { FundOutlined, AreaChartOutlined, LineChartOutlined, PieChartOutlined } from '@ant-design/icons'
import { ChartBarStacked, Map } from '@carbon/icons-react'
import lazyLoad from '@/router/utils/lazyLoad'
import { IRoute } from '@/typings/router'

const Charts: Array<IRoute> = [{
  path: '/charts',
  key: '/charts',
  label: '图表',
  name: '图表',
  icon: <AreaChartOutlined />,
  element: lazyLoad(lazy(() => import('@/layouts/index'))),
  redirect: '/charts/basic',
  children: [
    {
      path: '/charts/basic',
      key: '/charts/basic',
      label: '基础图表',
      title: '基础图表',
      name: 'charts-basic',
      icon: <FundOutlined />,
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "charts-basic" */ '@/pages/charts/basic'))),
    },
    {
      path: '/charts/line',
      key: '/charts/line',
      label: '折线图',
      title: '折线图',
      icon: <LineChartOutlined />,
      name: 'line',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "line" */ '@/pages/charts/line'))),
    },
    {
      path: '/charts/pie',
      key: '/charts/pie',
      label: '饼图',
      title: '饼图',
      icon: <PieChartOutlined />,
      name: 'pie',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "pie" */ '@/pages/charts/pie'))),
    },
    {
      path: '/charts/progress',
      key: '/charts/progress',
      label: '进度条',
      title: '进度条',
      icon: <ChartBarStacked />,
      name: 'progress',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "progress" */ '@/pages/charts/progress'))),
    },
    {
      path: '/charts/map',
      key: '/charts/map',
      label: '地图下钻',
      title: '地图下钻',
      icon: <Map />,
      name: 'map',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "map" */ '@/pages/charts/map'))),
    },
  ],
}]

export default Charts

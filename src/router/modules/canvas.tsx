import { lazy } from 'react'
import { FormatPainterOutlined } from '@ant-design/icons'
import { ColorPalette } from '@carbon/icons-react'
import lazyLoad from '@/router/utils/lazyLoad'
import { IRoute } from '@/typings/router'

const Canvas: Array<IRoute> = [{
  path: '/canvas',
  key: '/canvas',
  label: 'Canvas',
  name: 'Canvas',
  icon: <ColorPalette />,
  element: lazyLoad(lazy(() => import('@/layouts/index'))),
  redirect: '/canvas/draw',
  children: [
    {
      path: '/canvas/draw',
      key: '/canvas/draw',
      label: '画板',
      title: '画板',
      name: 'canvas-draw',
      icon: <FormatPainterOutlined />,
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "canvas-draw" */ '@/pages/canvas/draw'))),
    },
  ],
}]

export default Canvas

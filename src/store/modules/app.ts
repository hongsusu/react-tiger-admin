import { action, makeObservable, observable } from 'mobx'

class App {
  constructor() {
    makeObservable(this)
  }

  @observable menus = []

  @observable currentMenuList = []

  @observable theme = 'light'

  @observable collapsed = false

  // 设置菜单显示状态
  @action setCurrentMenuList = (list: any) => {
    this.currentMenuList = list
  }

  // 设置菜单显示状态
  @action setCollapsed = (type: boolean) => {
    this.collapsed = type
  }
}

export default new App()

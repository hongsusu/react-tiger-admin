import { AxiosResponse } from 'axios'

export interface ITimeTableItem {
  id?: string
  half?: number
  step?: number
  lessonstart?: string
  lessonend?: string
  semester_id?: string
}

export interface ISemesterBase{
  id?: string
  label?: string
  start?: string
  end?: string
  year?: number
  year_step?: number
  school_code?: string
  timetable: ITimeTableItem[]
}

export type ISemesterList = ISemesterBase[]

export interface StudentListParams {
  current?: number
  size?: number
  name?: string
  grade_code?: string
  class_id?: string
  sex?: string
  exam_uuid?: string
}

export interface SubjectOptions{
  [key: number]: string
}

export interface IExamPlacesDetail{
  exdate: any
  start_time: string
  morning_end: string
  afternoon_start: string
  subject: string
  room_code: string
  seat_num: number
  matches: number
  matches_uuid?: string
}

export interface IPlaces{
  area_code?: string
  exam_place_detail: IExamPlacesDetail[],
  exam_uuid?: string
  [prop: string]: any
}

export interface IExamStudent{
  [prop: string]: number
}

export interface IExamInfo{
  exam_student: IExamStudent
  exam_paper: IExamStudent
  exam_seat: IExamStudent
  exam_place_detail: IExamStudent
  max_seat?: number
}

export interface CustomAxiosResponse extends AxiosResponse {
  count?: string
}

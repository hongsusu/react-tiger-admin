import { useEffect, useState } from 'react'
import { Tooltip } from 'antd'
import classnames from 'classnames'
import './index.less'

const ProgressBar = (props: any) => {
  const { value, classes, label, title } = props
  const [step, setStep] = useState('')

  useEffect(() => {
    const v = (value / 100) * 100

    setStep(`${v}%`)
  }, [value])

  return (
    <div className="app-progress-bar">
      <Tooltip title={title}>
        <div className={classnames('progress', classes)} style={{ width: step }} />
      </Tooltip>
      <div className="name">{label}</div>
    </div>
  )
}

export default ProgressBar

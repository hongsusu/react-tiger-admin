import request from '@/utils/request'
// import { LoginInfo } from '@/typings/login'

export function fetchLogin(data: any) {
  return request({
    url: '/user/login',
    method: 'post',
    data,
  })
}

// 获取当前登录用户信息
export async function fetchUserInfo(params: any) {
  return request({
    url: '/user/info',
    method: 'get',
    params,
  })
}

// 用户列表
export function fetchUserList(params: any) {
  return request({
    url: '/user/list',
    method: 'get',
    params,
  })
}
